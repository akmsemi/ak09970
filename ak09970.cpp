#include "ak09970.h"
#include "ak09970_reg.h"
#include "Debug.h"

#define CONV16I(high,low)  ((int16_t)(((high) << 8) | (low)))

#define AK09970_WAIT_POWER_DOWN_US    100

AK09970::AK09970(){
    i2c = NULL;
    spi = NULL;
    cs = NULL;
}

AK09970::~AK09970(){
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
}

void AK09970::init(I2C *_i2c, SlaveAddress addr) {
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;

	i2c = _i2c;

    slaveAddress = addr;
}

void AK09970::init(SPI *_spi, DigitalOut *_cs) {
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
    spi=_spi;
    cs=_cs;
    cs->write(1);

    char buf = AK09970_VAL_I2CDIS;
    AK09970::write(AK09970_REG_I2CDIS, &buf, AK09970_LEN_REG_I2CDIS);
}

AK09970::Status AK09970::checkConnection() {
    AK09970::Status status = AK09970::SUCCESS;
    
    // Gets the WIA register value.
    char wiaValue[AK09970_LEN_MAX];

    if ((status=AK09970::read(AK09970_REG_WIA, wiaValue, AK09970_LEN_REG_WIA)) != SUCCESS) {
    	DBG_L1("I2C read error\r\n");
        return status;
    }

    // Checks the obtained value equals to the supposed value.
    if ( (wiaValue[0] != AK09970_VAL_WIA1) || (wiaValue[1] != AK09970_VAL_WIA2) ) {
        return AK09970::ERROR;
    }

    return status;
}

AK09970::Status AK09970::read(char registerAddress, char *buf, int length) {
    if(i2c != NULL){
        // Writes a start address
        if (i2c->write((slaveAddress << 1), &registerAddress, 1) != 0) {
        	DBG_L1("#Error: Failed to set address.\r\n");
            return AK09970::ERROR_I2C_WRITE;
        }
        
        // Reads register data
        if (i2c->read((slaveAddress << 1), buf, length) != 0) {
        	DBG_L1("#Error: Failed to read data.\r\n");
        	return AK09970::ERROR_I2C_READ;
        }
    }
    else if(spi != NULL){
      char tx_buf = registerAddress | 0x80;

      spi->lock();
      cs->write(0);
      spi->write(&tx_buf, 1, 0, 0);
      spi->write(0, 0, buf, length);
      cs->write(1);
      spi->unlock();
    }
    else{
    	DBG_L1("#Error: No SPI or I2C device found.\r\n");
        return AK09970::ERROR;        
    }
    return AK09970::SUCCESS;
}

AK09970::Status AK09970::write(char registerAddress, const char *buf, int length) {
    char data[AK09970_LEN_MAX];
    
    if(i2c != NULL){

    	// Creates data to be sent.
        data[0] = registerAddress;
        for (int i=0; i < length; i++) {
            data[1+i] = buf[i];
        }

        // Writes a start address. Doesn't send a stop condition.
        if (i2c->write((slaveAddress << 1), data, length + 1) != 0) {
            DBG_L1("#Error: Failed to set address.\r\n");
            return AK09970::ERROR_I2C_WRITE;
        }
    }
    else if(spi != NULL){
        spi->lock();

        for(int i = 0; i < length; i++)
        {
          data[0] = registerAddress+i;
          data[1] = buf[i];

          cs->write(0);
          spi->write(data, 2, 0, 0);
          cs->write(1);
        }

        spi->unlock();
    }
    else{
    	DBG_L1("#Error: No SPI or I2C device found.\r\n");
        return AK09970::ERROR;
    }

    return AK09970::SUCCESS;
}

AK09970::Status AK09970::isDataReady() {
    
	AK09970::Status status = AK09970::ERROR;
    char stValue[AK09970_LEN_MAX];
    
    // Gets the ST register value.
    if ((status=AK09970::read(AK09970_REG_ST, stValue, AK09970_LEN_REG_ST)) != AK09970::SUCCESS) {
        return status;
    }

    // Sets a return status corresponds to the obtained value.    
    if ((stValue[1] & AK09970_MASK_ST_DRDY) > 0) {
        status = AK09970::DATA_READY;
    } else {
        status = AK09970::NOT_DATA_READY;
    }
    return status;
}

AK09970::Status AK09970::getSwitchData(char *buf) {
    
	AK09970::Status status = AK09970::ERROR;

    // Gets the ST register value.
    if ((status=AK09970::read(AK09970_REG_ST, buf, AK09970_LEN_REG_ST)) != AK09970::SUCCESS) {
        // I2C read failed.
        return status;
    }
    
    return status;
}

AK09970::Status AK09970::setOperationMode(AK09970::OperationMode operationMode, AK09970::SensorDriveMode driveMode, AK09970::SensorMeasurementRange measureRange) {

	AK09970::Status status = AK09970::ERROR;
    
    // The device has to be put into power-down mode before switching mode.
    char buf = AK09970::MODE_POWER_DOWN;
    if (operationMode != AK09970::MODE_SINGLE_LOOP && operationMode != AK09970::MODE_POWER_DOWN) {
        if ((status=AK09970::write(AK09970_REG_CNTL2, &buf, AK09970_LEN_REG_CNTL2)) != AK09970::SUCCESS) {
            return status;
        }
        wait_us(AK09970_WAIT_POWER_DOWN_US);
    }
    
    if(operationMode == AK09970::MODE_SINGLE_LOOP)
    {
        operationMode = AK09970::MODE_SINGLE_MEASUREMENT;
    }
    buf = operationMode;
    buf |= (measureRange == AK09970::SENSOR_WIDE_MEASUREMENT_SETTING) ? AK09970_MASK_CNTL2_SMR : 0x00;
    buf |= (driveMode == AK09970::SENSOR_LOW_POWER_DRIVE) ? AK09970_MASK_CNTL2_SDR : 0x00;
    
    // Switch to the specified mode.
    if ((status=AK09970::write(AK09970_REG_CNTL2, &buf, AK09970_LEN_REG_CNTL2)) != AK09970::SUCCESS) {
        return status;
    }

    return AK09970::SUCCESS;
}

AK09970::Status AK09970::getOperationMode(AK09970::OperationMode *operationMode, AK09970::SensorDriveMode *driveMode, AK09970::SensorMeasurementRange *measureRange) {

	AK09970::Status status = AK09970::ERROR;

    char ctrl2 = 0x00;

    if ((status=AK09970::read(AK09970_REG_CNTL2, &ctrl2, AK09970_LEN_REG_CNTL2)) != AK09970::SUCCESS) {
        return status;
    }
    
    *operationMode = (AK09970::OperationMode)(ctrl2 & AK09970_MASK_CNTL2_OPMODE);
    *driveMode = (ctrl2 & AK09970_MASK_CNTL2_SDR) ? AK09970::SENSOR_LOW_POWER_DRIVE : AK09970::SENSOR_LOW_NOISE_DRIVE;
    *measureRange = (ctrl2 & AK09970_MASK_CNTL2_SMR) ? AK09970::SENSOR_WIDE_MEASUREMENT_SETTING : AK09970::SENSOR_HIGH_SENSITIVE_SETTING;
    
    return AK09970::SUCCESS;
}

AK09970::Status AK09970::getThreshold(AK09970::SensorAxis axis, AK09970::Threshold *th) {
    
	AK09970::Status status;
    char val[AK09970_LEN_THRESHOLD];
    char address; 

    if(axis == AK09970::AXIS_X)			address = AK09970_REG_TH1X;
    else if(axis == AK09970::AXIS_Y)	address = AK09970_REG_TH1Y;
    else if(axis == AK09970::AXIS_Z)	address = AK09970_REG_TH1Z;
    else return ERROR;
    
    status=read(address, val, AK09970_LEN_THRESHOLD);
    
    th->BOP1 = CONV16I((int16_t)val[0],val[1]);
    th->BRP1 = CONV16I((int16_t)val[2],val[3]);
    th->BOP2 = CONV16I((int16_t)val[4],val[5]);
    th->BRP2 = CONV16I((int16_t)val[6],val[7]);
    
    return status;
}

AK09970::Status AK09970::setThreshold(AK09970::SensorAxis axis, AK09970::Threshold th) {

	AK09970::Status status = AK09970::ERROR;
    char data[AK09970_LEN_THRESHOLD];
    char address;

    if(axis == AK09970::AXIS_X)			address = AK09970_REG_TH1X;
    else if(axis == AK09970::AXIS_Y)	address = AK09970_REG_TH1Y;
    else if(axis == AK09970::AXIS_Z)	address = AK09970_REG_TH1Z;
    else return ERROR;
    
    data[0] = (signed char)(th.BOP1 >> 8);
    data[1] = (signed char)(th.BOP1 & 0x00FF);
    data[2] = (signed char)(th.BRP1 >> 8);
    data[3] = (signed char)(th.BRP1 & 0x00FF);
    data[4] = (signed char)(th.BOP2 >> 8);
    data[5] = (signed char)(th.BOP2 & 0x00FF);
    data[6] = (signed char)(th.BRP2 >> 8);
    data[7] = (signed char)(th.BRP2 & 0x00FF);
    
    status=write(address, data, AK09970_LEN_THRESHOLD);
    return status;
}

AK09970::Status AK09970::getSwitchStatus(AK09970::SwitchStatus *sw_status, AK09970::ReadConfig config) {
    
//    Status status;
//
//    char address = getReadAddress(config);
//    const int len = getSTRegisterLength(address);
//
//    char val[AK09970_LEN_MAX];
//
//    if((status=read(address, val, len)) != SUCCESS) {
//        return status;
//    }
//    p->st_hi = val[0];
//    p->st_lo = val[1];
//
//    uint8_t hx_addr = 0;
//    uint8_t hy_addr = 0;
//    uint8_t hz_addr = 0;
//    uint8_t offset = 2;
//    int byte_len = 2;
//
//    int16_t xi = 0;
//    int16_t yi = 0;
//    int16_t zi = 0;
//
//    if(config.enabledUpperOnly) byte_len = 1;
//
//    // calculate address
//    if(config.enabledReadZ) hz_addr = offset;
//    if(config.enabledReadY) hy_addr = offset + (config.enabledReadZ ? byte_len : 0);
//    if(config.enabledReadX) hx_addr = offset + (config.enabledReadZ ? byte_len : 0) + (config.enabledReadY ? byte_len : 0);
//
//    // Calculates magnetic sensor value.
//    if(config.enabledUpperOnly){
//        if(config.enabledReadZ) zi = CONV16I( val[hz_addr], 0x00 );
//        if(config.enabledReadY) yi = CONV16I( val[hy_addr], 0x00 );
//        if(config.enabledReadX) xi = CONV16I( val[hx_addr], 0x00 );
//    }
//    else{
//        if(config.enabledReadZ) zi = CONV16I( val[hz_addr], val[hz_addr+1] );
//        if(config.enabledReadY) yi = CONV16I( val[hy_addr], val[hy_addr+1] );
//        if(config.enabledReadX) xi = CONV16I( val[hx_addr], val[hx_addr+1] );
//    }
    
    uint8_t hx_offset = 2;
    uint8_t hy_offset = 2;
    uint8_t hz_offset = 2;
    int16_t xi = 0;
    int16_t yi = 0;
    int16_t zi = 0;
    int sw_reg_len;
    int data_size = 2;			// Both upper and lower bytes, by default

    char switch_status[AK09970_LEN_MAX];
	char sw_reg_addr = AK09970_REG_ST;

	// Select register address and set axis byte offsets (see register map, Table 12.2)
    if(config.enabledUpperOnly){
    	sw_reg_addr |= AK09970_MASK_ADDRESS_UPPER_BITS;
    	data_size = 1;
    }
    if(config.enabledReadZ){
    	sw_reg_addr |= AK09970_MASK_ADDRESS_MAG_Z;
    	hx_offset += data_size;
    	hy_offset += data_size;
    }
    if(config.enabledReadY){
		sw_reg_addr |= AK09970_MASK_ADDRESS_MAG_Y;
		hx_offset += data_size;
    }
	if(config.enabledReadX){
		sw_reg_addr |= AK09970_MASK_ADDRESS_MAG_X;
	}

    // Determine the register length
    sw_reg_len = getSTRegisterLength(sw_reg_addr);

    // Read in the switch status values
    if(read(sw_reg_addr, switch_status, sw_reg_len) != SUCCESS)
    	return ERROR;

    sw_status->st_hi = switch_status[0];
    sw_status->st_lo = switch_status[1];

    // Concatenate magnetic sensor read values
    if(config.enabledUpperOnly){
        if(config.enabledReadZ) zi = CONV16I(switch_status[hz_offset], 0x00);
        if(config.enabledReadY) yi = CONV16I(switch_status[hy_offset], 0x00);
        if(config.enabledReadX) xi = CONV16I(switch_status[hx_offset], 0x00);
    }
    else{
        if(config.enabledReadZ) zi = CONV16I(switch_status[hz_offset], switch_status[hz_offset+1]);
        if(config.enabledReadY) yi = CONV16I(switch_status[hy_offset], switch_status[hy_offset+1]);
        if(config.enabledReadX) xi = CONV16I(switch_status[hx_offset], switch_status[hx_offset+1]);
    }
    
    sw_status->mag.mx = xi;
    sw_status->mag.my = yi;
    sw_status->mag.mz = zi;

    return SUCCESS;
}

AK09970::Status AK09970::setSwitchConfig(AK09970::SwitchConfig config) {
    
    Status status;
    char val[AK09970_LEN_MAX];
    char verifyData[AK09970_LEN_MAX];

	for(int i = 0; i < AK09970_LEN_REG_CNTL1; i++){
		val[i] = 0;
		verifyData[i] = 0;
	}
    
    if(config.enabledODINTEN)	val[0] |= AK09970_MASK_CNTL1_ODINTEN;
    if(config.enabledINTEN)		val[0] |= AK09970_MASK_CNTL1_INTEN;
    if(config.enabledERRADCEN)	val[0] |= AK09970_MASK_CNTL1_ERRADCEN;
    if(config.enabledERRXYEN)	val[1] |= AK09970_MASK_CNTL1_ERRXYEN;
    if(config.enabledSWZ2EN)	val[1] |= AK09970_MASK_CNTL1_SWZ2EN;
    if(config.enabledSWZ1EN)	val[1] |= AK09970_MASK_CNTL1_SWZ1EN;
    if(config.enabledSWY2EN)	val[1] |= AK09970_MASK_CNTL1_SWY2EN;
    if(config.enabledSWY1EN)	val[1] |= AK09970_MASK_CNTL1_SWY1EN;
    if(config.enabledSWX2EN)	val[1] |= AK09970_MASK_CNTL1_SWX2EN;
    if(config.enabledSWX1EN)	val[1] |= AK09970_MASK_CNTL1_SWX1EN;
    if(config.enabledDRDYEN)	val[1] |= AK09970_MASK_CNTL1_DRDYEN;
    
    if ((status=write(AK09970_REG_CNTL1, val, AK09970_LEN_REG_CNTL1)) != SUCCESS) {
        return status;
    }

    // Read back the data to verify it
    if ((status=read(AK09970_REG_CNTL1, verifyData, AK09970_LEN_REG_CNTL1)) != SUCCESS) {
    	return status;
    }

    for (int i = 0; i < AK09970_LEN_REG_CNTL1; i++) {
        if(val[i] != verifyData[i]) {
            return ERROR;
        }
    }

    return SUCCESS;
}

AK09970::Status AK09970::getSwitchConfig(AK09970::SwitchConfig *config) {
    Status status;
    char val[AK09970_LEN_MAX];

    for(int i = 0; i < AK09970_LEN_REG_CNTL1; i++){
    	val[i] = 0;
    }

    if ((status=read(AK09970_REG_CNTL1, val, AK09970_LEN_REG_CNTL1)) != SUCCESS) {
        // Failed to read.
        return status;
    }
    config->enabledODINTEN = ((val[0] & AK09970_MASK_CNTL1_ODINTEN) > 0) ? true : false;
    config->enabledINTEN = ((val[0] & AK09970_MASK_CNTL1_INTEN) > 0) ? true : false;
    config->enabledERRADCEN = ((val[0] & AK09970_MASK_CNTL1_ERRADCEN) > 0) ? true : false;
    config->enabledERRXYEN = ((val[1] & AK09970_MASK_CNTL1_ERRXYEN) > 0) ? true : false;
    config->enabledSWZ2EN = ((val[1] & AK09970_MASK_CNTL1_SWZ2EN) > 0) ? true : false;
    config->enabledSWZ1EN = ((val[1] & AK09970_MASK_CNTL1_SWZ1EN) > 0) ? true : false;
    config->enabledSWY2EN = ((val[1] & AK09970_MASK_CNTL1_SWY2EN) > 0) ? true : false;
    config->enabledSWY1EN = ((val[1] & AK09970_MASK_CNTL1_SWY1EN) > 0) ? true : false;
    config->enabledSWX2EN = ((val[1] & AK09970_MASK_CNTL1_SWX2EN) > 0) ? true : false;
    config->enabledSWX1EN = ((val[1] & AK09970_MASK_CNTL1_SWX1EN) > 0) ? true : false;
    config->enabledDRDYEN = ((val[1] & AK09970_MASK_CNTL1_DRDYEN) > 0) ? true : false;

    return status;
}

AK09970::Status AK09970::getPeriod(float *period) {
    Status status;
    char valCntl2 = 0;

    if ((status=read(AK09970_REG_CNTL2, &valCntl2, AK09970_LEN_REG_CNTL2)) != SUCCESS) {
        return status;
    }

    status = SUCCESS;

    char mode = valCntl2 & AK09970_MASK_CNTL2_OPMODE;
    if (mode == AK09970_MASK_CNTL2_MODE_POWER_DOWN) {
        *period = 0;
    } else if (mode == AK09970_MASK_CNTL2_MODE_SINGLE) {
        *period = 0;
    } else if (mode == AK09970_MASK_CNTL2_MODE_CONTINUOUS1) { // 0.25 Hz
        *period = 4.0;
    } else if (mode == AK09970_MASK_CNTL2_MODE_CONTINUOUS2) { // 0.5 Hz
        *period = 2.0;
    } else if (mode == AK09970_MASK_CNTL2_MODE_CONTINUOUS3) { // 1 Hz
        *period = 1.0;
    } else if (mode == AK09970_MASK_CNTL2_MODE_CONTINUOUS4) { // 10 Hz
        *period = 0.1;
    } else if (mode == AK09970_MASK_CNTL2_MODE_CONTINUOUS5) { // 20 Hz
        *period = 0.05;
    } else if (mode == AK09970_MASK_CNTL2_MODE_CONTINUOUS6) { // 50 Hz
        *period = 0.02;
    } else if (mode == AK09970_MASK_CNTL2_MODE_CONTINUOUS7) { // 100 Hz
        *period = 0.01;
    } else { // not in continuous mode
        status = ERROR;
    }
    
    return status;
}

AK09970::Status AK09970::reset() {
    Status status;
    char val = AK09970_VAL_SRST;

    if ((status=write(AK09970_REG_SRST, &val, AK09970_LEN_REG_SRST)) != SUCCESS) {
        return status;
    }

    return SUCCESS;
}

char AK09970::getReadAddress(AK09970::ReadConfig config) {
    char address = AK09970_REG_ST;
    if(config.enabledReadX) address |= AK09970_MASK_MAG_X;
    if(config.enabledReadY) address |= AK09970_MASK_MAG_Y;
    if(config.enabledReadZ) address |= AK09970_MASK_MAG_Z;
    if(config.enabledUpperOnly) address |= AK09970_MASK_ONLY_UPPER_BITS;

    return address;
}

int AK09970::getSTRegisterLength(char registerAddress) {

	if(registerAddress == AK09970_REG_ST)				return AK09970_LEN_REG_ST;
	if(registerAddress == AK09970_REG_ST_X)				return AK09970_LEN_REG_ST_X;
	if(registerAddress == AK09970_REG_ST_Y)				return AK09970_LEN_REG_ST_Y;
	if(registerAddress == AK09970_REG_ST_X_Y)			return AK09970_LEN_REG_ST_X_Y;
	if(registerAddress == AK09970_REG_ST_Z)				return AK09970_LEN_REG_ST_Z;
	if(registerAddress == AK09970_REG_ST_X_Z)			return AK09970_LEN_REG_ST_X_Z;
	if(registerAddress == AK09970_REG_ST_Y_Z)			return AK09970_LEN_REG_ST_Y_Z;
	if(registerAddress == AK09970_REG_ST_X_Y_Z)			return AK09970_LEN_REG_ST_X_Y_Z;
	if(registerAddress == AK09970_REG_UPPER_ST_X)		return AK09970_LEN_REG_UPPER_ST_X;
	if(registerAddress == AK09970_REG_UPPER_ST_Y)		return AK09970_LEN_REG_UPPER_ST_Y;
	if(registerAddress == AK09970_REG_UPPER_ST_X_Y)		return AK09970_LEN_REG_UPPER_ST_X_Y;
	if(registerAddress == AK09970_REG_UPPER_ST_Z)		return AK09970_LEN_REG_UPPER_ST_Z;
	if(registerAddress == AK09970_REG_UPPER_ST_X_Z)		return AK09970_LEN_REG_UPPER_ST_X_Z;
	if(registerAddress == AK09970_REG_UPPER_ST_Y_Z)		return AK09970_LEN_REG_UPPER_ST_Y_Z;
	if(registerAddress == AK09970_REG_UPPER_ST_X_Y_Z)	return AK09970_LEN_REG_UPPER_ST_X_Y_Z;

	return ERROR;

}
