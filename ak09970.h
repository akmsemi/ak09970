#ifndef AK09970_H
#define AK09970_H

#include "mbed.h"

/**
 * This is a device driver of AK09970.
 *
 * @note AK09970 is a 3-axis magnetic sensor device with programmable switch functions manufactured by AKM.
 * Example:
 * @code
 * #include "mbed.h"
 * #include "ak09970.h"
 * 
 * #define I2C_SPEED_100KHZ    100000
 * #define I2C_SPEED_400KHZ    400000
 * 
 * int main() {
 *     // Creates an instance of I2C
 *     I2C connection(I2C_SDA0, I2C_SCL0);
 *     connection.frequency(I2C_SPEED_100KHZ);
 *
 *     // TBD
 *
 *     while(true) {
 *              // TBD    
 *         }
 *     }
 * }
 * @endcode
*/
class AK09970
{
public:
    static const int AK09970_LEN_THRESHOLD = 8;
//    static const float AK09970_MAG_SENSITIVITY_HI = 1.1;    /**< High sensitivity setting [uT/LSB]*/
//    static const float AK09970_MAG_SENSITIVITY_LO = 3.2;    /**< Wide measurement range setting [uT/LSB]*/

    /** 
     * Possible slave address of AK09970. Selected by CAD1 and CAD0 pins. 
     */
    typedef enum {        
        SLAVE_ADDR_1 = 0x0C,    /**< CAD = Low */
        SLAVE_ADDR_2 = 0x0D,    /**< CAD = High */
    } SlaveAddress;
    
    /**
     * Available opration modes in AK09970.
     */
    typedef enum {
        MODE_POWER_DOWN = 0x00,         /**< Power-down mode */
        MODE_SINGLE_MEASUREMENT = 0x01, /**< Single measurement mode */
        MODE_CONTINUOUS_1 = 0x02,       /**< Continuous measurement mode 1, 0.25 Hz */
        MODE_CONTINUOUS_2 = 0x04,       /**< Continuous measurement mode 2, 0.5 Hz */
        MODE_CONTINUOUS_3 = 0x06,       /**< Continuous measurement mode 3, 1 Hz */
        MODE_CONTINUOUS_4 = 0x08,       /**< Continuous measurement mode 4, 10 Hz */
        MODE_CONTINUOUS_5 = 0x0A,       /**< Continuous measurement mode 5, 20 Hz */
        MODE_CONTINUOUS_6 = 0x0C,       /**< Continuous measurement mode 6, 50 Hz */
        MODE_CONTINUOUS_7 = 0x0E,       /**< Continuous measurement mode 7, 100 Hz */
        MODE_SINGLE_LOOP  = 0x11,       /**< Looping single measurement(Special feature for AKDP) */
    } OperationMode;

    /**
     * Sensor axis.
     */
    typedef enum {
        AXIS_X  = 0x00,     /**< Sensor X-axis */
        AXIS_Y  = 0x01,     /**< Sensor Y-axis */     
        AXIS_Z  = 0x02,     /**< Sensor Z-axis */
    } SensorAxis;
    
    /**
     * Sensor drive mode.
     */
    typedef enum {
        SENSOR_LOW_NOISE_DRIVE = 0x00,   /**< low-noise driver mode */
        SENSOR_LOW_POWER_DRIVE = 0x01,   /**< low-power drive mode */
    } SensorDriveMode;

    /**
     * Measurement range and sensitivity setting.
     */
    typedef enum {
        SENSOR_HIGH_SENSITIVE_SETTING   = 0x00,   /**< High sensitivity setting */
        SENSOR_WIDE_MEASUREMENT_SETTING = 0x01,   /**< Wide measurement range setting */
    } SensorMeasurementRange;
    
    /**
     * Status of function. 
     */
    typedef enum {
        SUCCESS,               /**< The function processed successfully. */
        ERROR_I2C_READ,        /**< Error related to I2C read. */
        ERROR_I2C_WRITE,       /**< Error related to I2C write. */
        ERROR,                 /**< General Error */
        DATA_READY,            /**< Data ready */
        NOT_DATA_READY,        /**< Data ready is not asserted. */
    } Status;
    
    /**
     * Configurations of data read.
     */
    typedef struct {
        bool enabledReadX;           /**< true if Read X axis data(=1). */
        bool enabledReadY;           /**< true if Read Y axis data(=1). */
        bool enabledReadZ;           /**< true if Read Z axis data(=1). */
        bool enabledUpperOnly;       /**< true if only read upper 8bit data(=1). */
    } ReadConfig;

    /**
     * Configurations of switch enable/disable.
     */
    typedef struct {
        bool enabledODINTEN;    /**< true if ODINTEN is enabled(=1). */
        bool enabledINTEN;      /**< true if INTEN is enabled(=1). */
        bool enabledERRADCEN;   /**< true if ERRADCEN is enabled(=1). */
        bool enabledERRXYEN;    /**< true if ERRXYEN is enabled(=1). */
        bool enabledSWZ2EN;     /**< true if SWZ2EN is enabled(=1). */
        bool enabledSWZ1EN;     /**< true if SWZ1EN is enabled(=1). */
        bool enabledSWY2EN;     /**< true if SWY2EN is enabled(=1). */
        bool enabledSWY1EN;     /**< true if SWY1EN is enabled(=1). */
        bool enabledSWX2EN;     /**< true if SWX2EN is enabled(=1). */
        bool enabledSWX1EN;     /**< true if SWX1EN is enabled(=1). */
        bool enabledDRDYEN;     /**< true if DRDYEN is enabled(=1). */
    } SwitchConfig;
    
    /**
     * Structure to hold a magnetic vector.
     */
    typedef struct {
        int16_t mx;        /**< x component */
        int16_t my;        /**< y component */
        int16_t mz;        /**< z component */
    } MagneticVector;        
    
    /**
     * Status of switch.
     */
    typedef struct {
//        bool isAssertedDOR;      /**< true if DOR is asserted(=1). */
//        bool isAssertedERRADC;   /**< true if ERRADC is asserted(=1). */
//        bool isAssertedERRXY;    /**< true if ERRXY is asserted(=1). */
//        bool isAssertedSWZ2;     /**< true if SWZ2 is asserted(=1). */
//        bool isAssertedSWZ1;     /**< true if SWZ1 is asserted(=1). */
//        bool isAssertedSWY2;     /**< true if SWY2 is asserted(=1). */
//        bool isAssertedSWY1;     /**< true if SWY1 is asserted(=1). */
//        bool isAssertedSWX2;     /**< true if SWX2 is asserted(=1). */
//        bool isAssertedSWX1;     /**< true if SWX1 is asserted(=1). */
//        bool isAssertedDRDY;     /**< true if DRDY is asserted(=1). */
        char st_hi;
        char st_lo;
        MagneticVector mag;
    } SwitchStatus;
    
    /**
     * Structure to hold threshold values.
     */
    typedef struct {
        int16_t BOP1;            /**< Operating Threshold 1 */
        int16_t BRP1;            /**< Returning Threshold 1 */
        int16_t BOP2;            /**< Operating Threshold 2 */
        int16_t BRP2;            /**< Returning Threshold 2 */
    } Threshold;
    
    /**
     * Structure to hold threshold values.
     */
//    typedef struct {
//        ThresholdElement thx;
//       ThresholdElement thy;
//        ThresholdElement thz;

//        float BOP1X;            /**< Operating Threshold of x-axis 1 */
//        float BRP1X;            /**< Returning Threshold of x-axis 1 */
//        float BOP2X;            /**< Operating Threshold of x-axis 2 */
//        float BRP2X;            /**< Returning Threshold of x-axis 2 */
//        float BOP1Y;            /**< Operating Threshold of y-axis 1 */
//        float BRP1Y;            /**< Returning Threshold of y-axis 1 */
//        float BOP2Y;            /**< Operating Threshold of y-axis 2 */
//        float BRP2Y;            /**< Returning Threshold of y-axis 2 */
//        float BOP1Z;            /**< Operating Threshold of z-axis 1 */
//        float BRP1Z;            /**< Returning Threshold of z-axis 1 */
//        float BOP2Z;            /**< Operating Threshold of z-axis 2 */
//        float BRP2Z;            /**< Returning Threshold of z-axis 2 */

//    } Threshold;
    
    /**
     * Constructor.
     *
     */
    AK09970();

    /**
     * Destructor.
     *
     */
    ~AK09970();
    

    /**
     * initialize for I2C interface. 
     *
     * @param _i2c pointer to the I2C instance.
     * @param addr I2C Slave address of the sensor.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    void init(I2C *_i2c, SlaveAddress addr);

    /**
     * initialize for SPI interface.
     *
     * @param _spi pointer to the SPI instance.
     * @param _cs DigitalOut pin for CS.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    void init(SPI *_spi, DigitalOut *_cs);

    /**
     * Check the connection. 
     *
     * @note Connection check is performed by reading a register which has a fixed value and verify it.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status checkConnection();

    /**
     * Sets switch configration.
     * @param config switch configration.
     * @return Returns SUCCESS when succeeded, otherwise returns other code.
     */
    Status setSwitchConfig(SwitchConfig config);

    /**
     * Gets switch configration.
     * @param config switch configration.
     * @return Returns SUCCESS when succeeded, otherwise returns other code.
     */
    Status getSwitchConfig(SwitchConfig *config);

    /**
     * Sets device operation mode.
     *
     * @param operationMode device opration mode
     * @param driveMode sensor drive mode. Low-power drive mode is default. 
     * @param measureRange sensor measurement range/sensitivity setting. High sensitivity setting is default. 
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status setOperationMode(OperationMode operationMode, SensorDriveMode driveMode = SENSOR_LOW_POWER_DRIVE, SensorMeasurementRange measureRange = SENSOR_HIGH_SENSITIVE_SETTING);

    /**
     * Gets device operation mode.
     *
     * @param operationMode device opration mode
     * @param driveMode sensor drive mode. Low-power drive mode is default. 
     * @param measureRange sensor measurement range/sensitivity setting. High sensitivity setting is default. 
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status getOperationMode(OperationMode *operationMode, SensorDriveMode *driveMode, SensorMeasurementRange *measureRange);

    /**
     * Returns if the specified switch is asserted.
     *
     * @param p structure of SwitchStatus
     * @param config switch configration.
     * 
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status getSwitchStatus(SwitchStatus *sw_status, ReadConfig config);
    
    /**
     * Gets magnetic vector from the device.
     *
     * @param vec Pointer to a instance of MagneticVector
     * @param p structure of SwitchStatus
     * @param config switch configration.
     * @param range sensor measurement range setting. High sensitivity is default.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
//    Status getMagneticVector(MagneticVector *vec, SwitchStatus *p, ReadConfig *config, SensorMeasurementRange range = SENSOR_HIGH_SENSITIVE_SETTING);

    /**
     * Sets a threshold values.
     * 
     * @param axis axis to set. x=0,y=1,z=2.
     * @param th Threshold.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status setThreshold(SensorAxis axis, Threshold th);
    
    /**
     * Gets the threshold values.
     *
     * @param axis axis to read. x=0,y=1,z=2.
     * @param p Pointer to an instance of Threshold object.
     *
     * @reutrn Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status getThreshold(SensorAxis axis, Threshold *th);
    
    /**
     * Sets the threshold values with a byte array.
     * @param buf Pointer to a buffer storing register values of threshold (THX, THY, THZ, TLX, TLY, TLZ). 
     * @return Returns SUCCESS when succeed, otherwise returns other value.
     */
//    Status setThreshold(const char *buf);

    /**
     * Gets the threshold values as a byte array.
     * @param buf Pointer to a buffer storing register values of threshold (THX, THY, THZ, TLX, TLY, TLZ). The buffer length must be more than LEN_THRESHOLD_BUF.
     * @return Returns SUCCESS when succeed, otherwise returns other value.
     */
//    Status getThreshold(char *buf);
    
    /**
     * Gets magnetic data, from the register HSX (0x20) to ST3 (0x23), from the device.
     *
     * @param buf buffer to store the data read from the device. 
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another value.
     */
    Status getSwitchData(char *buf);   
    
    /**
     * Gets current measurement period in sec.
     * @param period Pointer to the buffer to store the result
     * @return Returns SUCCESS when succeeded, otherwise returns another value.
     */
    Status getPeriod(float *period);

    /**
     * Resets the device.
     * @return Returns success when succeeded, otherwise returns other value.
     */    
    Status reset();
       
    /**
     * Check if data is ready, i.e. measurement is finished.
     *
     * @return Returns DATA_READY if data is ready or NOT_DATA_READY if data is not ready. If error happens, returns another code.
     */
    Status isDataReady();

    /**
     * Gets the magnetic data address to read from using the configuration.
     * @param config switch configuration.
     * @return read register address.
     */    
    char getReadAddress(ReadConfig config); 
    
    /**
     * Retrieves the length of the desired ST register.
     * @param registerAddress register address to be read.
     * @return register length of the address.
     */    
    int getSTRegisterLength(char registerAddress);
    
    /** 
     * Writes data to the device. 
     * @param registerAddress register address to be written
     * @param data data to be written
     * @param length of the data
     * @return Returns SUCCESS when succeeded, otherwise returns another value.
     */
    Status write(char registerAddress, const char *data, int length);

    /** 
     * Reads data from device. 
     * @param registerAddress register address to be read
     * @param buf buffer to store the read data
     * @param length bytes to be read
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status read(char registerAddress, char *buf, int length);
    
private:    
    I2C*			i2c;			/**< I2C communications object */
    SlaveAddress	slaveAddress;	/**< Slave address for the AK09970 */
    SPI*			spi;			/**< SPI communications object */
    DigitalOut*		cs;				/**< Chip Select pin */
    

};

#endif
