#ifndef AK09970__REG_H
#define AK09970__REG_H

// Register Map
#define AK09970_REG_WIA                 0x00
#define AK09970_REG_ST             0x10
#define AK09970_REG_ST_X           0x11
#define AK09970_REG_ST_Y           0x12
#define AK09970_REG_ST_X_Y         0x13
#define AK09970_REG_ST_Z           0x14
#define AK09970_REG_ST_X_Z         0x15
#define AK09970_REG_ST_Y_Z         0x16
#define AK09970_REG_ST_X_Y_Z       0x17
#define AK09970_REG_UPPER_ST             0x18
#define AK09970_REG_UPPER_ST_X           0x19
#define AK09970_REG_UPPER_ST_Y           0x1A
#define AK09970_REG_UPPER_ST_X_Y         0x1B
#define AK09970_REG_UPPER_ST_Z           0x1C
#define AK09970_REG_UPPER_ST_X_Z         0x1D
#define AK09970_REG_UPPER_ST_Y_Z         0x1E
#define AK09970_REG_UPPER_ST_X_Y_Z       0x1F
#define AK09970_REG_CNTL1               0x20
#define AK09970_REG_CNTL2               0x21
#define AK09970_REG_TH1X                0x22
#define AK09970_REG_TH2X                0x23
#define AK09970_REG_TH1Y                0x24
#define AK09970_REG_TH2Y                0x25
#define AK09970_REG_TH1Z                0x26
#define AK09970_REG_TH2Z                0x27
#define AK09970_REG_SRST                0x30
#define AK09970_REG_I2CDIS              0x31
#define AK09970_REG_TEST1               0x40
#define AK09970_REG_TEST2               0x41

// Bit Masks: ST1
#define AK09970_MASK_ST_DOR            0x02
#define AK09970_MASK_ST_ERRADC         0x01
#define AK09970_MASK_ST_ERRXY          0x80
#define AK09970_MASK_ST_SWZ2           0x40
#define AK09970_MASK_ST_SWZ1           0x20
#define AK09970_MASK_ST_SWY2           0x10
#define AK09970_MASK_ST_SWY1           0x08
#define AK09970_MASK_ST_SWX2           0x04
#define AK09970_MASK_ST_SWX1           0x02
#define AK09970_MASK_ST_DRDY           0x01

// Bit Masks: Magnetic Data
#define AK09970_MASK_MAG_X             0x01
#define AK09970_MASK_MAG_Y             0x02
#define AK09970_MASK_MAG_Z             0x04
#define AK09970_MASK_ONLY_UPPER_BITS   0x08

// Bit Masks: CNTL1 Register
#define AK09970_MASK_CNTL1_ODINTEN     0x04
#define AK09970_MASK_CNTL1_INTEN       0x02
#define AK09970_MASK_CNTL1_ERRADCEN    0x01
#define AK09970_MASK_CNTL1_ERRXYEN     0x80
#define AK09970_MASK_CNTL1_SWZ2EN      0x40
#define AK09970_MASK_CNTL1_SWZ1EN      0x20
#define AK09970_MASK_CNTL1_SWY2EN      0x10
#define AK09970_MASK_CNTL1_SWY1EN      0x08
#define AK09970_MASK_CNTL1_SWX2EN      0x04
#define AK09970_MASK_CNTL1_SWX1EN      0x02
#define AK09970_MASK_CNTL1_DRDYEN      0x01

// Bit Masks: CNTL2 Register
#define AK09970_MASK_CNTL2_OPMODE                0x0F       // Operation mode
#define AK09970_MASK_CNTL2_SMR                   0x20       // Measurement Range And Sensitivity Setting - 0: High sensitivity, 1: Wide measurement
#define AK09970_MASK_CNTL2_SDR                   0x10       // Sensor Drive Setting - 0: low-power, 1: low-noise
#define AK09970_MASK_CNTL2_MODE_POWER_DOWN       0x00       // power-down mode
#define AK09970_MASK_CNTL2_MODE_SINGLE           0x01       // single measurement
#define AK09970_MASK_CNTL2_MODE_CONTINUOUS1      0x02       // 0.25Hz
#define AK09970_MASK_CNTL2_MODE_CONTINUOUS2      0x04       // 0.5Hz
#define AK09970_MASK_CNTL2_MODE_CONTINUOUS3      0x06       // 1Hz
#define AK09970_MASK_CNTL2_MODE_CONTINUOUS4      0x08       // 10Hz
#define AK09970_MASK_CNTL2_MODE_CONTINUOUS5      0x0A       // 20Hz
#define AK09970_MASK_CNTL2_MODE_CONTINUOUS6      0x0C       // 50Hz
#define AK09970_MASK_CNTL2_MODE_CONTINUOUS7      0x0E       // 100Hz

// Bit Masks: Magnetic Data Addresses
#define AK09970_MASK_ADDRESS_MAG_X				0x01
#define AK09970_MASK_ADDRESS_MAG_Y				0x02
#define AK09970_MASK_ADDRESS_MAG_Z				0x04
#define AK09970_MASK_ADDRESS_UPPER_BITS			0x08

// Important Device Values
#define AK09970_VAL_WIA1                         0x48       // Company ID of AKM
#define AK09970_VAL_WIA2                         0xC0       // Device ID of AK09970
#define AK09970_VAL_SRST                         0x01       // Software reset
#define AK09970_VAL_I2CDIS                       0x1B       // I2C disable

// Register Lengths
#define	AK09970_LEN_REG_WIA					4
#define	AK09970_LEN_REG_ST					2
#define	AK09970_LEN_REG_ST_X				4
#define	AK09970_LEN_REG_ST_Y				4
#define	AK09970_LEN_REG_ST_X_Y				6
#define	AK09970_LEN_REG_ST_Z				4
#define	AK09970_LEN_REG_ST_X_Z				6
#define	AK09970_LEN_REG_ST_Y_Z				6
#define	AK09970_LEN_REG_ST_X_Y_Z			8
#define	AK09970_LEN_REG_UPPER_ST			2
#define	AK09970_LEN_REG_UPPER_ST_X			3
#define	AK09970_LEN_REG_UPPER_ST_Y			3
#define	AK09970_LEN_REG_UPPER_ST_X_Y		4
#define	AK09970_LEN_REG_UPPER_ST_Z			3
#define	AK09970_LEN_REG_UPPER_ST_X_Z		4
#define	AK09970_LEN_REG_UPPER_ST_Y_Z		4
#define	AK09970_LEN_REG_UPPER_ST_X_Y_Z		5
#define	AK09970_LEN_REG_CNTL1				2
#define	AK09970_LEN_REG_CNTL2				1
#define	AK09970_LEN_REG_TH1X				4
#define	AK09970_LEN_REG_TH2X				4
#define	AK09970_LEN_REG_TH1Y				4
#define	AK09970_LEN_REG_TH2Y				4
#define	AK09970_LEN_REG_TH1Z				4
#define	AK09970_LEN_REG_TH2Z				4
#define	AK09970_LEN_REG_SRST				1
#define	AK09970_LEN_REG_I2CDIS				1
#define	AK09970_LEN_REG_TEST1				2
#define	AK09970_LEN_REG_TEST2				1
#define AK09970_LEN_MAX						9		/**< Maximum value data to be written, plus an extra address byte. */

#endif        // AK09970__REG_H
